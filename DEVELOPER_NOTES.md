# Assumptions
- Trade currency by default is through Bitcoin and US Dollars. Making changing them dynamically is not part of the scope
- Using version 1 of the API
- No bar graph charts to appear upon clicking order book rows
- Can be readable through mobile
- Just a single page website displaying the orderbook and trade

# Things that have yet to be done
- The graph behind the orderbook table
- Graph zoom in/out
- Precision plus/minus
- Fixes from container margins and spacing from the table rows
- The date header on the trades
- Ticker is still incomplete and design still looks plain
- Mobile responsiveness is okay but needs to be improved
- I just improvised a Bitcoin logo but I could've dug deeper in the net to look for a white one


# Insights
- The API documentation is straightforward though it can be improved. 
- There is a bug on the playground on the right if using incognito on Google Chrome wherein the codes are all squeezed up to around 15px which becomes unreadable. Fortunately as a developer, I edited the CSS through debugger for the mean time and extended the width to 100% :P
- 8 hours is not enough for everything though the point of this challenge was to check the person's skills

# Suggestions/Comments

- I believe majority of the requirements have been accomplomplished

- For the trades, I have followed the one in the platform than on the mockups wherein it uses arrows than circles as indicator

- I woud've been best if there are sample outputs provided in the playground than just the format. Sure, there's a playground but for people who are opting for quick answers, it requires an extra effort to check information

- My next task was the precision but I didn't had enough time. Graphs might take a while

- I believe it would be best if the API returned an array of objects than a fixed element of an array. It's more readable and easy to call. Unless the array implementation was better, please let me know why is it a better data structure :)

- Overall, I loved the challenge! It gave me a better understanding of trading and it even got me more enthusiastic with the bitfinex platform. If in any case I would move forward, I can see myself doing this for the long term. Hoping for the best!