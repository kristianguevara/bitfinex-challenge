import React, { Component } from 'react';

import TradesUpdate from '../components/TradesUpdate';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

export default class Trades extends Component {

    constructor(props) {
        super(props);

        //Set default settings
        this.state = {
            settings: {
                event: 'subscribe',
                channel: 'trades',
                pair: 'BTCUSD'
            },
            skip: 0,
            limit: 25, //Display only 25 rows to be at-par with the order book and to comply with the existing platform
            tradeData: []
        }
    }

    componentDidMount() {
        let connection = new WebSocket(this.props.connectionString);

        connection.onmessage = (event) => { 
            let data = JSON.parse(event.data); //Let alone this be the validation if the data is an object or array
            this.evaluateData(data);
        };

        connection.onopen = () => {
            connection.send(JSON.stringify(this.state.settings));
        };
      
    }

    evaluateData = (data) => {
    
        //Data initialization goes here
        if (data.length === 2) {
            this.evaluateSnapshot(data[1]);
        } else if(data.length >= 4) {
            //Regular updates go here
            this.evaluateUpdates(data);
        }
        //Do nothing if else. It means it's not the response data we're looking fo. Likely the info response
    }

    evaluateUpdates = (update) => {
        let newTradeData = this.state.tradeData;
        newTradeData.unshift(update)
        newTradeData.pop()
        this.setState({
            tradeData: newTradeData
        });
    }

    evaluateSnapshot = (snapshot) => {
        //Just making sure we're not receiving the heartbeat string
        if(typeof snapshot === 'object') {
            //Assign snapshot data here//

            //Compared to orders, this one doesn't need division so immediately pass the data to the state
            this.setState({
                tradeData: snapshot
            });
        }
    }

    render() {
        return(
            <div className="feature-container">
                <section className="row">
                    <div className="col-md-7">
                        <a data-toggle="collapse" href="#trades">
                            <FontAwesomeIcon className="rotate accordion" icon={faChevronDown} />
                            <h3>Trades <span>BTC/USD</span></h3>
                        </a>
                    </div>
                    <div className="col-md-5">
                        <div className="market-container">
                            <p>Market</p>
                        </div>
                    </div>
                </section>
                <div id="trades" className="panel-collapse collapse show">
                    <TradesUpdate 
                        tradeData={this.state.tradeData}
                        skip={this.state.skip}
                        limit={this.state.limit}
                    />
                </div>
            </div>
        );
    }
}