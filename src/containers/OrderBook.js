import React, { Component } from 'react';

import OrderBookBids from '../components/OrderBookBids';
import OrderBookAsks from '../components/OrderBookAsks';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faMinus, faPlus,
     faSearchPlus, faSearchMinus } from '@fortawesome/free-solid-svg-icons';

export default class OrderBook extends Component {

    constructor(props) {
        super(props);

        //Set default settings
        this.state = {
            settings: {
                event: 'subscribe',
                channel: 'book',
                pair: 'BTCUSD',
                prec : 'P3',
                freq: 'F1',
                length: '25'
            },

            biddingData: [],
            askingData: []
        }
    }

    componentDidMount() {
        //Need this to be a different object so it has a different listener from other containers
        let connection = new WebSocket(this.props.connectionString);


        connection.onmessage = (event) => {
            let data = JSON.parse(event.data); //Let alone this be the validation if the data is an object or array
            this.evaluateData(data);
        };
  
        connection.onopen = () => {
            connection.send(JSON.stringify(this.state.settings));
        };
      
    }

    evaluateData = (data) => {
    
        //Data initialization goes here
        if (data.length === 2) {
            this.evaluateSnapshot(data[1]);
        } else if(data.length === 4) {
            //Regular updates go here
            this.evaluateUpdates(data);
        }
        //Do nothing if else. It means it's not the response data we're looking fo. Likely the info response
    }

    evaluateSnapshot = (snapshot) => {
        //Just making sure we're not receiving the heartbeat string
        if(typeof snapshot === 'object') {
            //Assign snapshot data here//

            //Since by default it returns 50 rows and half of each are bids/ask,
            //we only need to divide the data to quickly assign to another state
            //Fortunately, they are already sorted out because if not, we have to do more processing here

            const biddingData = snapshot.slice(0, snapshot.length / 2);
            const askingData = snapshot.slice(snapshot.length / 2, snapshot.length);
            this.setState({
                biddingData: biddingData,
                askingData: askingData
            });
        }
    }

    evaluateUpdates = (update) => {
        const amount = update[3];
        const price = update[1];

        //TODO: This unshifting and pop should be eliminated
        //Remove the existing Price then replace

        //Evaluate precision first
        if(amount < 0) {
            this.updateOrderBookRows(this.state.askingData, 1, update, price);
        } else {
            this.updateOrderBookRows(this.state.biddingData, 2, update, price);
        }
    }

    updateOrderBookRows = (row, kind, update, price) => {
        let kindProps = kind === 1 ? 'askingData' : 'biddingData';
        let index = row.findIndex(data => {
            return data[0] === price
        });

        this.setState(prevState => {
            const newData = [...prevState[kindProps] ];
            newData[index] = update;
            return { [kindProps] : newData }
        });
    }

    render() {
        return(
            <div className="feature-container">
                <section className="row">
                    <div className="col-md-7">
                        <a data-toggle="collapse" href="#orders">
                            <FontAwesomeIcon className="rotate accordion" icon={faChevronDown} />
                            <h3>Order Book <span>BTC/USD</span></h3>
                        </a>
                    </div>
                    <div className="col-md-5">
                        <div className="orderbook-controls-container">
                            <FontAwesomeIcon icon={faMinus} />
                            <FontAwesomeIcon icon={faPlus} />
                            <FontAwesomeIcon icon={faSearchMinus} />
                            <FontAwesomeIcon icon={faSearchPlus} />
                        </div>
                    </div>
                </section>
                <div id="orders" className="panel-collapse collapse show row">
                    <div className="col-lg-6">
                        <OrderBookBids biddingData={this.state.biddingData}/>
                    </div>
                    <div className="col-lg-6">
                        <OrderBookAsks askingData={this.state.askingData}/>
                    </div>
                </div>
            </div>
        );
    }
}