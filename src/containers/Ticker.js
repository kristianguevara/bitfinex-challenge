import React, { Component } from 'react';

import TickerDisplay from '../components/TickerDisplay';

export default class Ticker extends Component {

    constructor(props) {
        super(props);

        //Set default settings
        this.state = {
            settings: {
                event: 'subscribe',
                channel: 'ticker',
                pair: 'BTCUSD'
            },
            tickerData: [0,0,0,0,0,0,0,0,0,0,0]
        }
    }

    componentDidMount() {
        //Need this to be a different object so it has a different listener from other containers
        let connection = new WebSocket(this.props.connectionString);


        connection.onmessage = (event) => {
            let data = JSON.parse(event.data); //Let alone this be the validation if the data is an object or array
            console.log('data = ',data)
            this.evaluateData(data);
        };
  
        connection.onopen = () => {
            connection.send(JSON.stringify(this.state.settings));
        };
      
    }

    evaluateData = (data) => {
        //Data initialization and updates goes here
        if(data.length === 11) {
            this.evaluateUpdates(data);
        }
    }


    evaluateUpdates = (data) => {
        this.setState({
            tickerData: data
        });
    }

    render() {
        return(
            <TickerDisplay tickerData={this.state.tickerData}/>
        )
    }
}