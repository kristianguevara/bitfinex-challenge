import React, { Component } from 'react';
import './App.scss';

import OrderBook from './containers/OrderBook';
import Trades from './containers/Trades';
import Ticker from './containers/Ticker';

const webSocketApi = 'wss://api.bitfinex.com/ws/';
class App extends Component {

  componentDidMount() {
      let connection = new WebSocket(webSocketApi);
      this.setState({
        connection: connection
      });
  }

  render() {
    return (
      <div className="container row main">
        <div className="ticker-container-col">
          <Ticker connectionString={webSocketApi}/>
        </div>
        <div className="row main-container main-font-color">
          <div className="col-lg-6">
            <OrderBook connectionString={webSocketApi} />
          </div>
          <div className="col-lg-6">
            <Trades connectionString={webSocketApi} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
