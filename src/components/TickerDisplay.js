import React, { Component } from 'react';
import { faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class TickerDisplay extends Component {

    evaluateAmount = (amount) => {
        if(amount < 0) {
            return(
                <FontAwesomeIcon className="trade-amount-down" icon={faChevronDown} />
            );
        } else {
            return(
                <FontAwesomeIcon className="trade-amount-up" icon={faChevronUp} />
            );
        }
    }

    render() {
        const { tickerData } = this.props;
        return(
            <div className="row ticker-container">
                    <div className="col-lg-3">
                        <img src="http://pngimg.com/uploads/bitcoin/bitcoin_PNG6.png" alt="Bitcoin" className="btc-logo"/>
                    </div>
                    <div className="col-lg-9">
                        <div className="row">
                            <h5 className="col-sm-6">BTC/USD</h5>
                            <h6 className="col-sm-6">{tickerData[1]}</h6>
                        </div>
                        <div className="row">
                            <span className="col-sm-6">VOL {tickerData[8].toFixed(2)} USD</span>
                            <h6 className="col-sm-6">{this.evaluateAmount(tickerData[5])} {Math.abs(tickerData[5])} ({tickerData[6].toFixed(2)})%</h6>
                        </div>
                        <div className="row">
                            <h6 className="col-sm-6">LOW {tickerData[10]}</h6>
                            <h6 className="col-sm-6">HIGH {tickerData[9]}</h6>
                        </div>
                    </div>
                <div className="col-lg-9"></div>
            </div>
        )
    }
}