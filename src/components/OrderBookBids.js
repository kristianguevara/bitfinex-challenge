import React, { Component } from 'react';

export default class OrderBookBids extends Component {
    render() {
        const { biddingData } = this.props;

        return(
            <div>
                <table className="table table-borderless">
                    <thead>
                        <tr>
                            <th scope="col">Count</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Total</th>
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {biddingData.length === 0? (
                            <tr>
                                <th colSpan="4">Loading data please wait...</th>
                            </tr>): null
                        }
                        {
                            biddingData.map((data, key) => {

                                return (
                                    <tr key={key}>
                                        <td>{data[1]}</td>
                                        <td>{Math.abs(data[2].toFixed(2))}</td>
                                        <td>{Math.abs(data[2].toFixed(2))}</td>
                                        <td>{data[0]}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}