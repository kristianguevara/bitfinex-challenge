import React, { Component } from 'react';
import { faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class TradesUpdate extends Component {

    //Materialize row data here. Still does not defeat the purpose of a dumb component 
    //since it knows we're passing data but it's job to clean a little what it received
    convertDateToObject = (timestamp) => {
        return new Date(timestamp * 1000);
    }

    evaluateAmount = (amount) => {
        if(amount < 0) {
            return(
                <FontAwesomeIcon className="trade-amount-down" icon={faChevronDown} />
            );
        } else {
            return(
                <FontAwesomeIcon className="trade-amount-up" icon={faChevronUp} />
            );
        }
    }

    render() {
        const { tradeData, skip, limit } = this.props;

        return(
            <div>
                <table className="table table-borderless">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Time</th>
                            <th scope="col">Price</th>
                            <th scope="col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        { tradeData.length === 0? (
                            <tr>
                                <th colSpan="4">Loading data please wait...</th>
                            </tr>): null
                        }
                        {
                            tradeData.slice(skip, limit).map((data, key) => {
                                const dateTime = data[data.length - 3];
                                const price = data[data.length - 2];
                                const amount = data[data.length - 1];
                                return (
                                    <tr key={key}>
                                        <td>{ this.evaluateAmount(amount) }</td>
                                        <td>
                                            { this.convertDateToObject(dateTime).getHours() }:
                                            { this.convertDateToObject(dateTime).getMinutes() }:
                                            { this.convertDateToObject(dateTime).getSeconds() }
                                        </td>
                                        <td>{price}</td>
                                        <td>{Math.abs(amount).toFixed(4)}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}