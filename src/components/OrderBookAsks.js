import React, { Component } from 'react';

export default class OrderBookAsks extends Component {
    render() {
        const { askingData } = this.props;

        return(
            <div>
                <table className="table table-borderless">
                    <thead>
                        <tr>
                            <th scope="col">Price</th>
                            <th scope="col">Total</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        {askingData.length === 0? (
                            <tr>
                                <th colSpan="4">Loading data please wait...</th>
                            </tr>): null
                        }
                        {
                            askingData.map((data, key) => {

                                return (
                                    <tr key={key}>
                                        <td>{data[0]}</td>
                                        <td>{Math.abs(data[2].toFixed(2))}</td>
                                        <td>{Math.abs(data[2].toFixed(2))}</td>
                                        <td>{data[1]}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}